package modulo4;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingresar la primer variable");
		int PrimerValor =scan.nextInt();
		System.out.println("Ingresar la segunda variable");
		int SegundoValor =scan.nextInt();
		System.out.println("Ingresar la tercer variable");
		int TercerValor =scan.nextInt();
		
		if (PrimerValor > SegundoValor && SegundoValor > TercerValor)
			System.out.println("La primer variable " + PrimerValor + " es mayor");
		else if (SegundoValor > PrimerValor && SegundoValor > TercerValor)
			System.out.println("La segunda variable " + SegundoValor + " es mayor");
		else if (TercerValor > PrimerValor && TercerValor > SegundoValor)
			System.out.println("La tercer variable " + TercerValor + " es mayor");
		else
			System.out.println("No hay numero mayor");

		scan=null;
	
	}

}
