package modulo4;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		
		System.out.println("Ingresar el numero que quiere visualizar en la tabla");
		Scanner scan = new Scanner(System.in);
		int numero = scan.nextInt();
		int sumadepares = 0;
		
		System.out.println("la tabla a mostrar es " + numero);
		
		for(int i=1; i<11; i++ )
		{
			int resultado = numero * i;
			System.out.println(numero + "x" + i + "=" + resultado);
			if (resultado%2==0)
				sumadepares = sumadepares + resultado;
		}
		
		System.out.println("Suma de numeros pares = " + sumadepares);
		
		scan=null;


	}

}
