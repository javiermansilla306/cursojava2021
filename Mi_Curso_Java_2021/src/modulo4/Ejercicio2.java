package modulo4;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingresar un numero entero");
		int Numero = scan.nextInt();
		
		if (Numero%2==0)
			System.out.println("El numero es par");
		else
			System.out.println("El numero es impar");
		
		scan=null;


	}

}
