package modulo4;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		
		System.out.println("Ingresar el numero que quiere visualizar en la tabla");
		Scanner scan = new Scanner(System.in);
		int numero = scan.nextInt();
		
		System.out.println("la tabla a mostrar es " + numero);
		
		for(int i=0; i<10; i++ )
		{
			int resultado = numero * i;
			System.out.println(numero + "x" + i + "=" + resultado);
		}
		scan=null;

	}

}
