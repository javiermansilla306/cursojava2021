package modulo4;

import java.util.Scanner;

public class Ejercicio9 {

	public static void main(String[] args) {
	
		Scanner scan = new Scanner(System.in);
		System.out.println("Para la competicion los valores son: \n Piedra = 1 \n Papel = 2 \n Tijera = 3\n");
		System.out.println("Ingrese el valor del primer competidor");
		float PrimerCompetidor=scan.nextFloat();
		System.out.println("Ingrese el valor del segundo competidor");
		float SegundoCompetidor=scan.nextFloat();
		
		if (PrimerCompetidor == SegundoCompetidor)
			System.out.println("Empataron");
		else
		{
			if (PrimerCompetidor == 1 && SegundoCompetidor == 3)
				System.out.println("Gano el primer competidor");
			else if (PrimerCompetidor == 1 && SegundoCompetidor == 2)
				System.out.println("Gano el segundo competidor");
			else if (PrimerCompetidor == 2 && SegundoCompetidor == 1)
				System.out.println("Gano el primer competidor");
			else if (PrimerCompetidor == 2 && SegundoCompetidor == 3)
				System.out.println("Gano el segundo competidor");
			else if (PrimerCompetidor == 3 && SegundoCompetidor == 1)
				System.out.println("Gano el segundo competidor");
			else if (PrimerCompetidor == 3 && SegundoCompetidor == 2)
				System.out.println("Gano el primer competidor");
		}
		scan=null;
	



	}

}
