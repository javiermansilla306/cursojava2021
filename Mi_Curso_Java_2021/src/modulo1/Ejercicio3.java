package modulo1;

public class Ejercicio3 {

	public static void main(String[] args) {
		
		System.out.println("Tecla de escape \t Significado\n");
		System.out.println("\\n \t Significa una nueva linea\n");
		System.out.println("\\t \t Significa un tab de espacio\n");
		System.out.println("\\\" \t Significa un tab de espacio\n");
		System.out.println("\\\\ \t Significa un tab de espacio\n");
		System.out.println("\\' \t Significa un tab de espacio\n");


	}

}
