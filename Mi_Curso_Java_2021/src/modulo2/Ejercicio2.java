package modulo2;

public class Ejercicio2 {

	public static void main(String[] args) {
	
		byte      bmin = -128;
	    byte      bmax = 127;
	    short     smin = -32768;
	    short     smax = 32767;
	    int       imax = -2147483648;
	    int       imin = 2147483647;
	    long      lmin = -9223372036854775808L;
	    long      lmax = 9223372036854775807L;
	
		System.out.println("Tipo\t Minimo\t\t\t Maximo\n");
		System.out.println("----\t ------\t\t\t ------\n");
		System.out.println("Byte\t -128 \t\t\t  127\n");
		System.out.println("Short\t -32768\t\t\t 32767\n");
		System.out.println("Int\t -2147483648\t\t 2147483647\n");
		System.out.println("Long\t -9223372036854775808\t 9223372036854775807\n");
		System.out.println("La formula para calcular los minimos es 2*(cant de bits) y maximos es = 2*(cant de bits)-1");



	}

}
